import { expect } from "chai";

export function checkStatusCode(response, statusCode: 200 | 201 | 204 | 400 | 401 | 403 | 404 | 409 | 500) {
    expect(response.statusCode, `Status Code should be ${statusCode}`).to.equal(statusCode);
}

export function checkResponseTime(response, maxResponseTime: number = 3000) {
    expect(response.timings.phases.total, `Response time should be less than ${maxResponseTime}ms`).to.be.lessThan(
        maxResponseTime
    );
}

export function checkSchema(response, schema) {
    expect(response.body).to.be.jsonSchema(schema); 
}

export function checkId(response, userId) {
    expect(response.body.user.id, `ID should be ${userId}`).to.be.equal(userId, "ID isn't correct");
}

export function checkEmail(response, email) {
    expect(response.body.user.email, `Email should be ${email}`).to.be.equal(email, "Email isn't correct");
}

export function checkUserName(response, userName) {
    expect(response.body.user.userName, `Username should be ${userName}`).to.be.equal(userName, "Username isn't correct"); 
}

export function checkUserData(response, userData: object) {
    expect(response.body).to.be.deep.equal(userData, "User details isn't correct");
}


