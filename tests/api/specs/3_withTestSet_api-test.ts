import {
    checkResponseTime,
    checkStatusCode, 
} from '../../helpers/functionsForChecking.helper';
import { UsersController } from '../lib/controllers/users.controller';
const users = new UsersController();

describe('Use test data set for searching user by id', () => {
    let invalidIDsDataSet = [" ", "d", "D", "д", "Д", "!", ""];

    invalidIDsDataSet.forEach((id) => {
        it(`should not find user with invalid id : '${id}'`, async () => {
            let response = await users.getUserById(id);

            checkStatusCode(response, 400); 
            checkResponseTime(response, 3000);
        });
    });
});

