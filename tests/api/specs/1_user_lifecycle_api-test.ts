import { expect } from "chai";
import { UsersController } from "../lib/controllers/users.controller";
import { AuthController } from "../lib/controllers/auth.controller";
import { RegisterController } from "../lib/controllers/register.controller";
import { checkEmail, checkId, checkResponseTime, checkSchema, checkStatusCode, checkUserData, checkUserName } from "../../helpers/functionsForChecking.helper";


const register = new RegisterController();
const users = new UsersController();
const auth = new AuthController();

const schemas = require('./data/schemas_testData.json');
const chai = require('chai');
chai.use(require('chai-json-schema'));

const emailValue = global.appConfig.users.Samuel.email;
const avatarValue = global.appConfig.users.Samuel.avatar;
const userNameValue = global.appConfig.users.Samuel.userName;
const passwordValue = global.appConfig.users.Samuel.password;

describe("User`s lifecycle", () => {
    let userId: number;
    let accessToken: string;
    let userRegistrationData: object;
    let userDataBeforeUpdate, userDataToUpdate;

    it(`Register`, async () => {
        userRegistrationData = {
            id: 0,
            avatar: avatarValue,
            email: emailValue,
            userName: userNameValue,
            password: passwordValue
        };

        let response = await register.register(userRegistrationData);
        checkStatusCode(response, 201);
    });

    it(`should return 200 status code and all users when getting the user collection`, async () => {
        let response = await users.getAllUsers();

        checkStatusCode(response, 200);
        checkSchema(response, schemas.schema_allUsers);
        checkResponseTime(response, 1000);
    });

    it(`should be able to login and get the token`, async () => {
        let response = await auth.login(emailValue, passwordValue);
        
        checkStatusCode(response, 200);
        checkSchema(response, schemas.schema_userWithToken);

        userId = response.body.user.id;
        accessToken = response.body.token.accessToken.token;
               
        checkId(response, userId);
        checkEmail(response, emailValue);
        checkUserName(response, userNameValue);
        checkResponseTime(response, 1000);
        
    });

    it(`should return correct details of the currect user`, async () => {
        let response = await users.getCurrentUser(accessToken);

        checkStatusCode(response, 200);
        checkSchema(response, schemas.schema_userWithoutToken);
        expect(response.body.id, `ID should be ${userId}`).to.be.equal(userId, "ID isn't correct");
        expect(response.body.email, `Email should be ${emailValue}`).to.be.equal(emailValue, "Email isn't correct");
        expect(response.body.userName, `Username should be ${userNameValue}`).to.be.equal(userNameValue, "Username isn't correct"); 

        userDataBeforeUpdate = response.body;

        checkResponseTime(response, 1000);
    });

    it(`should update username using valid data`, async () => {
        // replace the last 3 characters of actual username with random characters.
        // Another data should be without changes
        function replaceLastThreeWithRandom(str: string): string {
            return str.slice(0, -3) + Math.random().toString(36).substring(2, 5);
        }

        userDataToUpdate = {
            id: userDataBeforeUpdate.id,
            avatar: userDataBeforeUpdate.avatar,
            email: userDataBeforeUpdate.email,
            userName: replaceLastThreeWithRandom(userDataBeforeUpdate.userName),
        };

        let response = await users.updateUser(userDataToUpdate, accessToken);
        checkStatusCode(response, 204);
        checkResponseTime(response, 1000);
    });

    it(`should return correct user details of the currect user after updating`, async () => {
        let response = await users.getCurrentUser(accessToken);

        checkStatusCode(response, 200);
        checkResponseTime(response, 1000);
        checkUserData(response, userDataToUpdate);
    });

    it(`should return user details when getting user details with valid id`, async () => {       
        let response = await users.getUserById(userId);

        checkStatusCode(response, 200);
        checkUserData(response, userDataToUpdate);
        checkResponseTime(response,1000);
    });


    it(`Delete user`, async () => {
        let response = await users.deleteUserById(userId, accessToken);
        checkStatusCode(response, 204);
    });
});