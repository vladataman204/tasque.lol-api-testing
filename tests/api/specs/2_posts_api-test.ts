import { expect } from "chai";
import { UsersController } from "../lib/controllers/users.controller";
import { AuthController } from "../lib/controllers/auth.controller";
import { RegisterController } from "../lib/controllers/register.controller";
import { checkEmail, checkId, checkResponseTime, checkSchema, checkStatusCode, checkUserData, checkUserName } from "../../helpers/functionsForChecking.helper";
import { PostsController } from "../lib/controllers/posts.controller";


const register = new RegisterController();
const users = new UsersController();
const auth = new AuthController();
const posts = new PostsController();

const schemas = require('./data/schemas_testData.json');
const chai = require('chai');
chai.use(require('chai-json-schema'));

const emailValue = global.appConfig.users.Samuel.email;
const avatarValue = global.appConfig.users.Samuel.avatar;
const userNameValue = global.appConfig.users.Samuel.userName;
const passwordValue = global.appConfig.users.Samuel.password;
const post = "My test post!"

describe("Posts controller", () => {
    let userId: number;
    let postId: number;
    let accessToken: string;
    let userRegistrationData: object;
    let userPostData, userLikeData;
    
    before(`Register`, async () => {
        userRegistrationData = {
            id: 0,
            avatar: avatarValue,
            email: emailValue,
            userName: userNameValue,
            password: passwordValue
        };

        let responseReg = await register.register(userRegistrationData);
        let responseAuth = await auth.login(emailValue, passwordValue);

        userId = responseAuth.body.user.id;
        accessToken = responseAuth.body.token.accessToken.token;
    });

    it(`should return 400 status code when user adds new post with empty request body`, async () => {
        userPostData = {};

        let response = await posts.addNewPost(userPostData, accessToken);
        checkStatusCode(response, 400);
    }); 
       
    it(`should return 200 status code when user adds new post`, async () => {
        userPostData = {
            authorId: userId,
            previewImage: "",
            body: post
        };

        let response = await posts.addNewPost(userPostData, accessToken);

        checkStatusCode(response, 200);
        checkSchema(response, schemas.schema_newPost);
        
        postId = response.body.id;

        expect(response.body.author.id, `Author Id should be ${userId}`).to.be.equal(userId, "Author Id isn't correct");
        expect(response.body.body, `Post body should be ${post}`).to.be.equal(post, "Post body isn't correct");
        
        checkResponseTime(response, 1000);
    }); 
    
    it("created post should exist", async function () {
        let response = await posts.getAllPosts();
        let postCreated = response.body.some(function (element) {
            return element.id === postId;
        });
        expect(postCreated).to.be.true;
        checkStatusCode(response, 200);
        checkResponseTime(response, 1000);
    });

    it(`should return 400 status code when user adds like to a post with empty request body`, async () => {
        let response = await posts.addLikeToPost(userLikeData, accessToken);

        checkStatusCode(response, 400);
        checkResponseTime(response, 1000);
    });

    it(`should return 200 status code when user adds like to a post`, async () => {
        userLikeData = {
            entityId: postId,
            isLike: true,
            userId: userId
        };

        let response = await posts.addLikeToPost(userLikeData, accessToken);

        checkStatusCode(response, 200);
        checkResponseTime(response, 1000);
    });

    it("created post should be liked", async function () {
        let response = await posts.getAllPosts();
        let likeAdded = response.body.some(function (element) {
            if (element.id === postId) {
                return element.reactions.some(function (reaction) {
                    return reaction.isLike === true && reaction.user.id === userId;
                });
            }
        });
        expect(likeAdded).to.be.true;
        checkStatusCode(response, 200);
        checkResponseTime(response, 1000);
    });

    it(`should return 200 status code when user adds like to a post`, async () => {
        let response = await posts.addLikeToPost(userLikeData, accessToken);

        checkStatusCode(response, 200);
        checkResponseTime(response, 1000);
    });
  
    after(`Delete user`, async () => {
        let response = await users.deleteUserById(userId, accessToken);
    });
});