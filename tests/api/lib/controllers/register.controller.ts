import { ApiRequest } from "../request";

const baseUrl: string = global.appConfig.baseUrl;

export class RegisterController {
    async register(userData: object) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("POST")
            .url(`api/Register`)
            .body(userData)
            .send();
        return response;
    }
}
